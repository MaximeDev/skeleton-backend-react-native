const mongoose = require("mongoose");

const MONGODB_URL = "mongodb+srv://test:test@testcluster-81jvj.mongodb.net/<dbname>?retryWrites=true&w=majority";

mongoose.connect(MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

module.exports = { mongoose };
