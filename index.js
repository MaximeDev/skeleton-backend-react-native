const express = require("express");
const bodyParser = require("body-parser");

const { mongoose } = require('./db/db');
const userController = require("./controllers/userController");

const app = express();
const port = 3333;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/user", userController);



app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});
